/* global require, describe, it, sinon */
/* jshint expr: true */
'use strict';

describe('productList view', function(){
    var view, models;

    beforeEach(function(done){
        require(
            [
                'app/views/productList',
                'app/collections/product',
                'app/models/product'
            ],
            function(ProductListView, ProductCollection, ProductModel) {
                models = [ new ProductModel(), new ProductModel() ];
                var collection = new ProductCollection(models);
                view = new ProductListView({ collection: collection });
                done();
            }
        );
    });

    it('should be a div', function() {
        expect(view.tagName).to.equal('div');
    });

    it('should bind to its container', function() {
        expect(view.$el.selector).to.equal('#article-list-container');
    });

    it('should render the view when an item is added to the collection', function() {
        sinon.spy(view, 'listenTo');
        view.initialize();
        expect(view.listenTo).to.have.been.calledWith(view.collection, 'add', view.render);
    });

    it('should render its template', function() {
        sinon.stub(view, 'template', function() { return 'aRenderedTemplate'; });
        sinon.spy(view.$el, 'html');
        view.render();
        expect(view.template).to.have.been.called;
        expect(view.$el.html).to.have.been.calledWith('aRenderedTemplate');
        view.$el.html.restore();
        view.template.restore();
    });

    it('should append the rendered members of the collection', function() {
        //TODO: stub/mock dependencies (child view creation).
        var appendSpy = sinon.stub();
        sinon.stub(view, '$', function() { return { append: appendSpy }; });
        view.render();
        expect(view.$).to.have.always.been.calledWith('tbody');
        expect(appendSpy).to.have.been.calledTwice;
        view.$.restore();
    });
});

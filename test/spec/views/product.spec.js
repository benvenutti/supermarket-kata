/* global require, describe, it, sinon */
/* jshint expr: true */
'use strict';

describe('product view', function(){
    var model, view, unitPriceFormatter, eventAggregator;

    beforeEach(function(done){
        require(
            [
                'app/views/product',
                'app/models/product',
                'app/views/helpers/unitPriceFormatter',
                'app/views/helpers/eventAggregator'
            ],
            function(ProductView, ProductModel, _unitPriceFormatter_, _eventAggregator_) {
                model = new ProductModel({ id: 1, name: 'aProduct', price: { id: 2, quantity: 123, price: 12345 }, discount: {} });
                view = new ProductView({ model: model });
                unitPriceFormatter = _unitPriceFormatter_;
                eventAggregator = _eventAggregator_;
                done();
            }
        );
    });

    it('should be a tr', function() {
        expect(view.tagName).to.equal('tr');
    });

    it('should render the view when the model changes', function() {
        sinon.spy(view, 'listenTo');
        view.initialize();
        expect(view.listenTo).to.have.been.calledWith(view.model, 'change', view.render);
        view.listenTo.restore();
    });

    it('should notify when the buy button is clicked', function() {
        view.initialize();
        expect(view.events['click a.add-to-cart']).to.equal('addItemToCart');
    });

    it('should broadcast the add to cart event', function() {
        var itemData;
        sinon.stub(eventAggregator, 'trigger');
        view.addItemToCart();
        itemData = {
            description: view.model.get('name'),
            quantity: view.quantity,
            productId: view.model.get('id'),
            price: view.model.get('price'),
            discount: view.model.get('discount')
        };
        expect(eventAggregator.trigger).to.have.been.calledWith('cart:adding', itemData);
        eventAggregator.trigger.restore();
    });

    it('should render the model through its template', function() {
        sinon.stub(model, 'toJSON', function() { return 'someData'; });
        sinon.stub(view, 'template', function() { return 'renderedData'; });
        sinon.spy(view.$el, 'html');
        view.render();
        expect(model.toJSON).to.have.been.called;
        expect(view.template).to.have.been.calledWith('someData');
        expect(view.$el.html).to.have.been.calledWith('renderedData');
        view.$el.html.restore();
        view.template.restore();
        model.toJSON.restore();
    });

    it('should format prices', function() {
        var formattedPrice;
        var price = {};
        var units = 'XX';
        sinon.stub(unitPriceFormatter, 'format', function(){
            return 'formattedPrice';
        });
        formattedPrice = view.formatPrice(price, units);
        expect(unitPriceFormatter.format).to.have.been.calledWith(price, units);
        expect(view.formatPrice(price)).to.equal('formattedPrice');
        unitPriceFormatter.format.restore();
    });

    it('should define default quantity based on price definition', function() {
        view.render();
        expect(view.quantity).to.equal(123);
    });

    it('should listen to changes on quantity input', function() {
        expect(view.events['change input']).to.equal('setQuantity');
    });

    it('should not set the quantity from the form if the input is invalid', function() {
        view.setQuantity({ currentTarget: 'invalid' });
        expect(view.quantity).to.equal(123);
     });
});

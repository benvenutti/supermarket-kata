/* global require, describe, it, sinon */
/* jshint expr: true */
'use strict';

describe('cartItem view', function(){
    var model, view, priceFormatter;

    beforeEach(function(done){
        require(
            [
                'app/views/cartItem',
                'app/models/cartItem',
                'app/views/helpers/priceFormatter'
            ],
            function(CartView, CartItemModel, _priceFormatter_) {
                model = new CartItemModel();
                view = new CartView({ model: model });
                priceFormatter = _priceFormatter_;
                done();
            }
        );
    });

    it('should be a tr', function() {
        expect(view.tagName).to.equal('tr');
    });

    it('should render the view when the model changes', function() {
        sinon.spy(view, 'listenTo');
        view.initialize();
        expect(view.listenTo).to.have.been.calledWith(view.model, 'change', view.render);
    });

    it('should render the model through its template', function() {
        sinon.stub(model, 'toJSON', function() { return 'someData'; });
        sinon.stub(view, 'template', function() { return 'renderedData'; });
        sinon.spy(view.$el, 'html');
        view.render();
        expect(model.toJSON).to.have.been.called;
        expect(view.template).to.have.been.calledWith('someData');
        expect(view.$el.html).to.have.been.calledWith('renderedData');
        view.remove();
        view.unbind();
    });

    it('should format the totals for the model', function() {
        sinon.stub(view.model, 'get', function(attribute) {
            var values = {
                totalPrice: function() { return 123.45; }
            };
            return values[attribute];
        });
        sinon.stub(priceFormatter, 'format', function() { return 'formattedPrice'; });
        expect(view.totalPrice()).to.equal('formattedPrice');
        expect(view.model.get).to.have.been.calledWith('totalPrice');
        expect(priceFormatter.format).to.have.been.calledWith(123.45);
        priceFormatter.format.restore();
        view.model.get.restore();
    });
});

/* global require, describe, it, sinon */
'use strict';

describe('unit price formatter', function() {
    var unitPriceFormatter, priceFormatter;

    beforeEach(function(done) {
        require(
            [
                'app/views/helpers/unitPriceFormatter',
                'app/views/helpers/priceFormatter'
            ],
            function(_unitPriceFormatter_, _priceFormatter_) {
                unitPriceFormatter = _unitPriceFormatter_;
                priceFormatter = _priceFormatter_;
                done();
            }
        );
    });

    describe('for unitary prices', function() {
        it('should format prices as pounds', function() {
            var price = { price: 123, quantity: 1 };
            sinon.stub(priceFormatter, 'format', function() { return '$4.56'; });
            expect(unitPriceFormatter.format(price, 'unit')).to.equal('$4.56');
            expect(priceFormatter.format).to.have.been.calledWith(123);
            priceFormatter.format.restore();
        });
    });

    describe('for group prices', function() {
        it('should display price quantity', function() {
            var price = { price: 123, quantity: 3 };
            expect(unitPriceFormatter.format(price, 'unit')).to.equal('£1.23/3');
        });
    });

    describe('for group prices with different units', function() {
        it('should display price quantity', function() {
            var price = { price: 123, quantity: 3 };
            expect(unitPriceFormatter.format(price, 'xx')).to.equal('£1.23/3xx');
        });
    });
});

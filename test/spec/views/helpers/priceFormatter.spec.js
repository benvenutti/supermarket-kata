/* global require, describe, it */
'use strict';

describe('price formatter', function() {
    var priceFormatter;

    beforeEach(function(done) {
        require(
            [
                'app/views/helpers/priceFormatter'
            ],
            function(_priceFormatter_) {
                priceFormatter = _priceFormatter_;
                done();
            }
        );
    });

    it('should format prices as pounds', function() {
        expect(priceFormatter.format(129.6)).to.equal('£1.30');
    });

});

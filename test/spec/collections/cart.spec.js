/* global require,  describe, it, sinon */
/* jshint expr: true */
'use strict';

describe('cart collection', function(){
	var collection, eventAggregator, CartItem;

	beforeEach(function(done){
		require(
			[
				'app/collections/cart',
				'app/views/helpers/eventAggregator',
				'app/models/cartItem'
			],
			function(CartCollection, _eventAggregator_, _CartItem_) {
				collection = new CartCollection();
				eventAggregator = _eventAggregator_;
				CartItem = _CartItem_;
				done();
			}
		);
	});

	it('should listen to Add To Cart event', function() {
		sinon.spy(eventAggregator, 'on');
		collection.initialize();
		expect(eventAggregator.on).to.have.been.calledWith('cart:adding', collection.addToCart, collection);
		eventAggregator.on.restore();
	});

	it('should add an element to the collection', function() {
		//TODO: stub dependencies
		var product = {
			description: 'description'
		};
		sinon.spy(collection, 'add');
		collection.addToCart(product);
		expect(collection.add).to.have.been.called;
		collection.add.restore();
	});

	it('should update quantity if adding an already existing element to the collection', function() {
		//TODO: stub dependencies
		var product = {
			description: 'description',
			quantity: 3
		};
		var getStub = function() { return 10; };
		var setSpy = sinon.spy();
		sinon.spy(collection, 'add');
		sinon.stub(collection, 'findWhere');
		collection.findWhere.onCall(0).returns(null);
		collection.findWhere.onCall(1).returns({ get: getStub, set: setSpy });
		collection.addToCart(product);
		collection.addToCart(product);
		expect(collection.add).to.have.been.calledOnce;
		expect(collection.findWhere).to.have.been.calledTwice;
		expect(setSpy).to.have.been.calledWith({ quantity: 13 });
		collection.findWhere.restore();
		collection.add.restore();
	});

});

/* global require, describe, it */
/* jshint expr: true */
'use strict';

describe('product collection', function(){
    var ProductCollection;

    beforeEach(function(done){
        require(
            [
                'app/collections/product'
            ],
            function(_ProductCollection_) {
                ProductCollection = _ProductCollection_;
                done();
            }
        );
    });

    it('should have a collection url', function() {
        var collection = new ProductCollection();
        expect(collection.url).to.equal('/api/products.json');
    });
});

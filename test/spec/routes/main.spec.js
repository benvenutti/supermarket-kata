/* global require, describe, it */
/* jshint expr: true */
/* jshint -W024 */
'use strict';

describe('main router', function(){
	var mainRouter;

	beforeEach(function(done){
		require(
			[
				'../../scripts/routes/main'
			],
			function(MainRouter) {
				mainRouter = new MainRouter();
				done();
			}
		);
	});

	it('should define a default route', function() {
		expect(mainRouter.routes['*default']).to.not.be.undefined;
	});
});

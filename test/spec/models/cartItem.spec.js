/* global require, describe, it, sinon */
/* jshint expr: true */
'use strict';

describe('cartItem model', function() {
    var cartItem;

    beforeEach(function(done) {
        require(
            [
                'app/models/cartItem'
            ],
            function(CartItemModel) {
                cartItem = new CartItemModel();
                done();
            }
        );
    });

    it('should define a priceTotal attribute', function() {
    	sinon.spy(cartItem, 'set');
    	sinon.stub(cartItem, 'totalPriceFunction', function() { return 'aFunction'; });
    	cartItem.initialize();
    	expect(cartItem.set).to.have.been.called;
    	expect(cartItem.set).to.have.been.calledWith('totalPrice', 'aFunction');
    	expect(cartItem.totalPriceFunction).to.have.been.calledWith(cartItem);
    	cartItem.totalPriceFunction.restore();
    	cartItem.set.restore();
    });

    it('should calculate the totals for the quantity and price', function() {
        sinon.stub(cartItem, 'get', function(attribute) {
            var values = {
                quantity: 3,
                price: {
                    quantity: 2,
                    price: 45
                }
            };
            return values[attribute];
        });
        expect(cartItem.totalPriceFunction(cartItem)()).to.equal(67.5);
        cartItem.get.restore();
    });

    it('should apply discounts on the total', function() {
        sinon.stub(cartItem, 'get', function(attribute) {
            var values = {
                quantity: 6,
                price: {
                    quantity: 1,
                    price: 45
                },
                discount: {
                	exactQuantity: 4,
                	price: 10
                }
            };
            return values[attribute];
        });
        expect(cartItem.totalPriceFunction(cartItem)()).to.equal(100);
        cartItem.get.restore();
    });
});

/*global define*/

define([
    'jquery',
    'backbone',
    'app/collections/product',
    'app/views/productList',
    'app/collections/cart',
    'app/views/cart'
], function ($, Backbone, ProductCollection, ProductListView, CartCollection, CartView) {
    'use strict';

    var MainRouter = Backbone.Router.extend({
        routes: {
        	'*default': 'home'
        },

        home: function() {
        	var products = new ProductCollection();
        	var productList = new ProductListView({ collection: products });
        	productList.render();
        	products.fetch();

            var cartItems = new CartCollection();
            var cart = new CartView({collection: cartItems});
            cart.render();
        }
    });

    return MainRouter;
});

/*global require*/
'use strict';

require.config({
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/lodash/dist/lodash',
        bootstrap: '../bower_components/sass-bootstrap/dist/js/bootstrap',
        app: '.'
    }
});

require([
    'backbone',
    'app/routes/main'
], function (Backbone, MainRouter) {
    var router;
    router = new MainRouter();
    Backbone.history.start();
});

/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'app/views/helpers/priceFormatter'
], function ($, _, Backbone, JST, priceFormatter) {
    'use strict';

    var CartItemView = Backbone.View.extend({
        template: JST['app/scripts/templates/cartItem.ejs'],

        tagName: 'tr',

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        totalPrice: function () {
            var total = this.model.get('totalPrice')();
            return priceFormatter.format(total);
        }
    });

    return CartItemView;
});

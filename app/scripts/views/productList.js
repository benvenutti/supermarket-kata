/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'app/views/product'
], function ($, _, Backbone, JST, ProductView) {
    'use strict';

    var ProductListView = Backbone.View.extend({
        el: $('#article-list-container'),

        template: JST['app/scripts/templates/productList.ejs'],

        events: {},

        initialize: function () {
            this.listenTo(this.collection, 'add', this.render);
        },

        render: function () {
            this.$el.html(this.template());
            this.collection.each(function(product) {
                var productView = new ProductView({ model: product });
                this.$('tbody').append(productView.render().el);
            }, this);
        }
    });

    return ProductListView;
});

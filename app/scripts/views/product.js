/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'app/views/helpers/unitPriceFormatter',
    'app/views/helpers/eventAggregator'
], function ($, _, Backbone, JST, unitPriceFormatter, eventAggregator) {
    'use strict';

    var ProductView = Backbone.View.extend({
        template: JST['app/scripts/templates/product.ejs'],

        tagName: 'tr',

        events: {
            'click a.add-to-cart': 'addItemToCart',
            'change input': 'setQuantity'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render, this);
        },

        render: function () {
            if (!this.quantity) {
                this.quantity = this.model.get('price').quantity;
            }
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        formatPrice: function(price, units) {
            return unitPriceFormatter.format(price, units);
        },

        addItemToCart: function() {
            var itemData = {
                description: this.model.get('name'),
                quantity: this.quantity,
                productId: this.model.get('id'),
                price: this.model.get('price'),
                discount: this.model.get('discount')
            };
            eventAggregator.trigger('cart:adding', itemData);
        },

        setQuantity: function(event) {
            var newQuantity = parseInt($(event.currentTarget).val(), 10);
            if (!isNaN(newQuantity)) {
                this.quantity = newQuantity;
            }
            this.render();
        }
    });

    return ProductView;
});

/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'app/views/cartItem'
], function ($, _, Backbone, JST, CartItemView) {
    'use strict';

    var CartView = Backbone.View.extend({
        el: $('#shopping-cart'),

        template: JST['app/scripts/templates/cart.ejs'],

        events: {},

        initialize: function () {
            this.listenTo(this.collection, 'add', this.render);
        },

        render: function () {
            this.$el.html(this.template());
            this.collection.each(function(cartItem) {
                var cartItemView = new CartItemView({ model: cartItem });
                this.$('tbody').append(cartItemView.render().el);
            }, this);
        }
    });

    return CartView;
});

/*global define*/

define([
		'app/views/helpers/priceFormatter'
	],
	function (priceFormatter) {
		'use strict';

		return {
			format: function (price, units) {
				var formattedPrice = priceFormatter.format(price.price);
				if ( price.quantity!==1 ) {
					formattedPrice += '/' + price.quantity;
				}
				if (units!=='unit') {
					formattedPrice += units;
				}
				return formattedPrice;
			}
		};
	}
);

/*global define, Math*/

define([], function () {
	'use strict';

	return {
		format: function (price) {
			return '£' + parseFloat(Math.round(price*100/100)/100).toFixed(2);
		}
	};
});

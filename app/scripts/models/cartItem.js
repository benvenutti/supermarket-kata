/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var CartItemModel = Backbone.Model.extend({
    	initialize: function() {
    		this.set('totalPrice', this.totalPriceFunction(this));
    	},

    	totalPriceFunction: function(self) {
    		return function() {
                var price, discount, quantity, discountedTotal, discountGroupItems;
                discountedTotal = 0;
                price = self.get('price');
                discount = self.get('discount');
                quantity = self.get('quantity');
                if (discount) {
                    discountGroupItems = Math.floor(quantity / discount.exactQuantity);
                    discountedTotal = discountGroupItems * discount.price;
                    quantity = quantity % discount.exactQuantity;
                }
                return discountedTotal + quantity * price.price / price.quantity;
    		};
    	}
    });

    return CartItemModel;
});

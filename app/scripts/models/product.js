/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var ProductModel = Backbone.Model.extend({
        url: '',

        defaults: {
            price: {},
            units: 'unit'
        }
    });

    return ProductModel;
});

/*global define*/

define([
    'underscore',
    'backbone',
    'app/models/cartItem',
    'app/views/helpers/eventAggregator'
], function (_, Backbone, CartItem, eventAggregator) {
    'use strict';

    var CartCollection = Backbone.Collection.extend({
        model: CartItem,

        initialize: function() {
        	eventAggregator.on('cart:adding', this.addToCart, this);
        },

        addToCart: function(itemData) {
            var existingItem;
            existingItem = this.findWhere({ productId: itemData.productId });
            if (existingItem) {
                existingItem.set({quantity: itemData.quantity + existingItem.get('quantity')});
            } else {
    		    this.add(new CartItem(itemData));
            }
        }
    });

    return CartCollection;
});

/*global define*/

define([
    'underscore',
    'backbone',
    'app/models/product'
], function (_, Backbone, ProductModel) {
    'use strict';

    var ProductCollection = Backbone.Collection.extend({
        url: '/api/products.json',

        model: ProductModel
    });

    return ProductCollection;
});

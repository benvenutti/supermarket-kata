/* jshint expr: true */
/* jshint -W024 */
'use strict';

var expect = require('chai').expect;

var stepDefinitions = function () {
    this.World = require('../support/world.js').World;

    this.Then(/^I see a shopping cart$/, function (callback) {
        expect(this.shoppingCart.headingText()).to.equal('Shopping Cart:');
        callback();
    });

    this.Then(/^I see an informative message about my empty cart$/, function(callback) {
        expect(this.shoppingCart.emptyCart.text()).to.match(/^\s*Your shopping cart is empty. Please add some products.\s*$/);
        callback();
    });

    this.Then(/^I don't see an informative message about my empty cart$/, function(callback) {
        expect(this.shoppingCart.emptyCart.present()).to.be.false;
        callback();
    });

    this.Then(/^I see the product on the cart$/, function(callback) {
        expect(this.shoppingCart.item.get(1).description()).to.equal('Apple');
        callback();
    });

    this.Then(/^I see the product quantity (updated )?on the cart$/, function(updated, callback) {
        var expectedQuantity = '100';
        if (updated) {
            expectedQuantity = '200';
        }
        expect(this.shoppingCart.item.get(1).quantity()).to.equal(expectedQuantity);
        callback();
    });

    this.Then(/^I see the total price for the quantity on the cart$/, function(callback) {
        expect(this.shoppingCart.item.get(1).totalPrice()).to.equal('£3.98');
        callback();
    });

    this.Then(/^I see the "(.*)" discount applied on the price on the cart$/, function(discount, callback) {
        var discountedPrices = {
            '3 per £1': '£1.40',
            'Buy 3 Pay 2': '£2.10'
        }
        expect(this.shoppingCart.item.get(1).totalPrice()).to.equal(discountedPrices[discount]);
        callback();
    });
};

module.exports = stepDefinitions;

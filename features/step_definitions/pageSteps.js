'use strict';

var stepDefinitions = function () {
    this.World = require('../support/world.js').World;

    this.Given(/^I am on the supermarket kata page$/, function (callback) {
        this.visit('http://localhost:9002', callback);
    });
};

module.exports = stepDefinitions;

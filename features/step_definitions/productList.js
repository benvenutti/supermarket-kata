'use strict';

var expect = require('chai').expect;

var stepDefinitions = function () {
    this.World = require('../support/world.js').World;

    this.When(/^I click on the "Buy!" button for the (.*) product(?: again)?$/, function(position, callback) {
        var positions = {
            first: 0,
            third: 2,
            fourth: 3
        };
        this.productList.product.buttons.click(positions[position], callback);
    });

    this.Then(/^I see a list with the available products$/, function (callback) {
        expect(this.productList.headingText()).to.equal('Available products:');
        callback();
    });

    this.Then(/^I see the names of the available products$/, function (callback) {
        var expectedValues = [ 'Apple', 'Bread', 'Cheese', 'Fizzy Drink' ];
        var names = this.productList.product.names();
        expect(names).to.deep.equal(expectedValues);
        callback();
    });

    this.Then(/^I see the prices of the available products$/, function (callback) {
        var expectedValues = [ '£0.40', '£1.25', '£1.99/100g.', '£0.70' ];
        var prices = this.productList.product.prices();
        expect(prices).to.deep.equal(expectedValues);
        callback();
    });

    this.Then(/^I can specify how many units to add for each article$/, function (callback) {
        var expectedValues = [ '1', '1', '100', '1' ];
        var quantities = this.productList.product.quantities();
        expect(quantities).to.deep.equal(expectedValues);
        callback();
    });

    this.Then(/^I see "Buy" buttons for each article$/, function(callback) {
        var buttonsText = this.productList.product.buttons.text();
        expect(buttonsText).to.deep.equal([ 'Buy!', 'Buy!', 'Buy!', 'Buy!' ]);
        callback();
    });
};

module.exports = stepDefinitions;

'use strict';

var Zombie = require('zombie');

var World = function World(callback) {
    var browser = new Zombie();
    var textValue = function(node) { return node.childNodes[0].nodeValue; };

    this.browser = browser;

    this.visit = function (url, callback) {
        browser.visit(url, callback);
    };

    this.productList = (function() {
        var productListSelector = '#article-list-container';
        var productRowsSelector = productListSelector + ' table tbody tr';
        var tableColumn = function(column) { return productRowsSelector + ' td:nth-child(' + column + ')'; };
        var quantityInputSelector = tableColumn(3) + ' input';
        var buyButtonsSelector = tableColumn(4) + ' a.btn';
        var inputValue = function(node) { return node.value; };

        return {
            headingText: function() { return textValue(browser.query(productListSelector + ' h4')); },
            product: {
                names: function() { return browser.queryAll(tableColumn(1)).map(textValue); },
                prices: function() { return browser.queryAll(tableColumn(2)).map(textValue); },
                quantities: function() { return browser.queryAll(quantityInputSelector).map(inputValue); },
                buttons: {
                    click: function(index, callback) {
                        browser.fire(browser.queryAll(buyButtonsSelector)[index], 'click', callback);
                    },
                    text: function() { return browser.queryAll(buyButtonsSelector).map(textValue); }
                }
            }
        };
    })();

    this.shoppingCart = (function() {
        var shoppingCartSelector = '#shopping-cart';
        var itemRowSelector = function(index) { return shoppingCartSelector + ' table tbody tr:nth-child(' + index + ')'; };

        return {
            headingText: function() { return textValue(browser.query(shoppingCartSelector + ' h4')); },
            emptyCart: {
                text: function() { return textValue(browser.query(shoppingCartSelector + ' div.alert')); },
                present: function() { return browser.queryAll(shoppingCartSelector + ' div.alert').length === 1; }
            },
            item: {
                get: function(index) {
                    return {
                        description: function() { return textValue(browser.querySelector(itemRowSelector(index) + ' td:nth-child(2)')); },
                        quantity: function() { return textValue(browser.querySelector(itemRowSelector(index) + ' td:nth-child(1)')); },
                        totalPrice: function() { return textValue(browser.querySelector(itemRowSelector(index) + ' td:nth-child(3)')); }
                    };
                }
            }
        };
    })();

    callback();
};
exports.World = World;

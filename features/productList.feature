Feature: Product list
    As a customer
    I want to see a list of products
    So that I can buy my groceries

    Scenario: Product list
        Given I am on the supermarket kata page
        Then I see a list with the available products

    Scenario: Product names
        Given I am on the supermarket kata page
        Then I see the names of the available products

    Scenario: Product prices
        Given I am on the supermarket kata page
        Then I see the prices of the available products

    Scenario: Product quantities
        Given I am on the supermarket kata page
        Then I can specify how many units to add for each article

    Scenario: Buy buttons
        Given I am on the supermarket kata page
        Then I see "Buy" buttons for each article

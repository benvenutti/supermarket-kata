Feature: Shopping Cart
    As a customer
    I want to have a shopping cart
    So that I can buy my groceries

    Scenario: Shopping cart
        Given I am on the supermarket kata page
        Then I see a shopping cart

    Scenario: Empty cart message
        Given I am on the supermarket kata page
        Then I see an informative message about my empty cart

    Scenario: Adding to cart
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the first product
        Then I see the product on the cart

    Scenario: No empty message for cart with products
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the first product
        Then I don't see an informative message about my empty cart

    Scenario: Adding to cart
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the third product
        Then I see the product quantity on the cart

    Scenario: Updating quantities for existing articles
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the third product
        And I click on the "Buy!" button for the third product again
        Then I see the product quantity updated on the cart

    Scenario: Total prices
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the third product
        And I click on the "Buy!" button for the third product again
        Then I see the total price for the quantity on the cart

    Scenario: "3 per £1" discounts
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the first product
        And I click on the "Buy!" button for the first product again
        And I click on the "Buy!" button for the first product again
        And I click on the "Buy!" button for the first product again
        Then I see the "3 per £1" discount applied on the price on the cart

    Scenario: "Buy 3 pay 2" discounts
        Given I am on the supermarket kata page
        When I click on the "Buy!" button for the fourth product
        And I click on the "Buy!" button for the fourth product again
        And I click on the "Buy!" button for the fourth product again
        And I click on the "Buy!" button for the fourth product again
        Then I see the "Buy 3 Pay 2" discount applied on the price on the cart

# Supermarket kata
The applicaton has been scaffolded using yeoman, and it is using require.js to manage dependencies.

Unit test (mocha) can be found under `test/spec`
End to End test (cucumberjs) can be found under `features`

#ToDo

Some dependencies have not been tested due to sinon.js inability to mock them.
testr.js (https://github.com/mattfysh/testr.js) could be used to address that.

## Requirements
- nodejs and npm
- grunt: `npm install -g grunt`
- bower: `npm install -g bower`

## Installation instructions
- install build dependencies `npm install`
- install run-time dependencies `bower install && cd test && bower install && cd ..`
- (optional) run the tests `grunt test`
- run the application: `grunt serve`
